(function() {

	// Slideshow
	var flexslider = $('.flexslider')
	flexslider.flexslider({
		animation: "fade",
		animationLoop: true,
		startAt: 0,
		slideshow: true,
		slideshowSpeed: 5000,
		animationspeed: 1000,
		controlNav: false
	});

})();