## 101
This project was built with 960 Grid System, Laravel, jQuery (and Flexslider), Glyphicons(free), and it uses MySQL for its databases.

## ?
This is the first project I've ever done with Laravel (probably the first project I've done with PHP, too). The site is currently live @ [here].

[**[Preview](http://i40.tinypic.com/w0khfr.jpg)**]
