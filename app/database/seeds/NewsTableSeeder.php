<?php

class NewsTableSeeder extends Seeder {
	public function run() {

		$data = array(
			array(
				'title'			=>	'Hello World',
				'body'			=>	'<p>Aenean metus nunc, ultricies et lobortis a, elementum lobortis eros. Vestibulum id nibh ut augue laoreet rhoncus ultricies vel enim. Curabitur dignissim neque placerat, tempor est at, adipiscing arcu. Aliquam erat volutpat. Maecenas blandit dignissim dui ac varius. Fusce non fringilla leo. Maecenas ac pulvinar massa. Aliquam sed tellus non est ultricies laoreet. Phasellus sit amet pharetra sem, quis suscipit tellus. Fusce lorem massa, tempor eu magna vel, luctus placerat lorem. </p> <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas volutpat mauris et ipsum luctus venenatis. Nunc ut elit a risus hendrerit elementum consectetur id ipsum. Sed lobortis pharetra purus, at sollicitudin lectus venenatis aliquam. Nullam rhoncus lacus lacus, id vulputate neque volutpat eget. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin dignissim vel mauris in consectetur. Vestibulum pharetra lectus eu porta consectetur. Nulla consectetur libero odio, quis vehicula magna aliquam nec. Donec scelerisque tellus ac risus eleifend tincidunt.</p>',
				'user_id'		=>	1,
				'created_at'	=>	new DateTime,
				'updated_at'	=>	new DateTime
			),

			array(
				'title'			=>	'Hello World',
				'body'			=>	'<p>Aenean metus nunc, ultricies et lobortis a, elementum lobortis eros. Vestibulum id nibh ut augue laoreet rhoncus ultricies vel enim. Curabitur dignissim neque placerat, tempor est at, adipiscing arcu. Aliquam erat volutpat. Maecenas blandit dignissim dui ac varius. Fusce non fringilla leo. Maecenas ac pulvinar massa. Aliquam sed tellus non est ultricies laoreet. Phasellus sit amet pharetra sem, quis suscipit tellus. Fusce lorem massa, tempor eu magna vel, luctus placerat lorem. </p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas volutpat mauris et ipsum luctus venenatis. Nunc ut elit a risus hendrerit elementum consectetur id ipsum. Sed lobortis pharetra purus, at sollicitudin lectus venenatis aliquam. Nullam rhoncus lacus lacus, id vulputate neque volutpat eget. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin dignissim vel mauris in consectetur. Vestibulum pharetra lectus eu porta consectetur. Nulla consectetur libero odio, quis vehicula magna aliquam nec. Donec scelerisque tellus ac risus eleifend tincidunt.</p>',
				'user_id'		=>	2,
				'created_at'	=>	new DateTime,
				'updated_at'	=>	new DateTime
			),

			array(
				'title'			=>	'Hello World',
				'body'			=>	'<p>Aenean metus nunc, ultricies et lobortis a, elementum lobortis eros. Vestibulum id nibh ut augue laoreet rhoncus ultricies vel enim. Curabitur dignissim neque placerat, tempor est at, adipiscing arcu. Aliquam erat volutpat. Maecenas blandit dignissim dui ac varius. Fusce non fringilla leo. Maecenas ac pulvinar massa. Aliquam sed tellus non est ultricies laoreet. Phasellus sit amet pharetra sem, quis suscipit tellus. Fusce lorem massa, tempor eu magna vel, luctus placerat lorem. </p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas volutpat mauris et ipsum luctus venenatis. Nunc ut elit a risus hendrerit elementum consectetur id ipsum. Sed lobortis pharetra purus, at sollicitudin lectus venenatis aliquam. Nullam rhoncus lacus lacus, id vulputate neque volutpat eget. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin dignissim vel mauris in consectetur. Vestibulum pharetra lectus eu porta consectetur. Nulla consectetur libero odio, quis vehicula magna aliquam nec. Donec scelerisque tellus ac risus eleifend tincidunt.</p>',
				'user_id'		=>	1,
				'created_at'	=>	new DateTime,
				'updated_at'	=>	new DateTime
			),

			array(
				'title'			=>	'Hello World',
				'body'			=>	'<p>Aenean metus nunc, ultricies et lobortis a, elementum lobortis eros. Vestibulum id nibh ut augue laoreet rhoncus ultricies vel enim. Curabitur dignissim neque placerat, tempor est at, adipiscing arcu. Aliquam erat volutpat. Maecenas blandit dignissim dui ac varius. Fusce non fringilla leo. Maecenas ac pulvinar massa. Aliquam sed tellus non est ultricies laoreet. Phasellus sit amet pharetra sem, quis suscipit tellus. Fusce lorem massa, tempor eu magna vel, luctus placerat lorem. </p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas volutpat mauris et ipsum luctus venenatis. Nunc ut elit a risus hendrerit elementum consectetur id ipsum. Sed lobortis pharetra purus, at sollicitudin lectus venenatis aliquam. Nullam rhoncus lacus lacus, id vulputate neque volutpat eget. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin dignissim vel mauris in consectetur. Vestibulum pharetra lectus eu porta consectetur. Nulla consectetur libero odio, quis vehicula magna aliquam nec. Donec scelerisque tellus ac risus eleifend tincidunt.</p>',
				'user_id'		=>	1,
				'created_at'	=>	new DateTime,
				'updated_at'	=>	new DateTime
			)
		);

		News::create(array(
				'title'			=>	'Hello World',
				'body'			=>	'<p>Aenean metus nunc, ultricies et lobortis a, elementum lobortis eros. Vestibulum id nibh ut augue laoreet rhoncus ultricies vel enim. Curabitur dignissim neque placerat, tempor est at, adipiscing arcu. Aliquam erat volutpat. Maecenas blandit dignissim dui ac varius. Fusce non fringilla leo. Maecenas ac pulvinar massa. Aliquam sed tellus non est ultricies laoreet. Phasellus sit amet pharetra sem, quis suscipit tellus. Fusce lorem massa, tempor eu magna vel, luctus placerat lorem. </p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas volutpat mauris et ipsum luctus venenatis. Nunc ut elit a risus hendrerit elementum consectetur id ipsum. Sed lobortis pharetra purus, at sollicitudin lectus venenatis aliquam. Nullam rhoncus lacus lacus, id vulputate neque volutpat eget. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin dignissim vel mauris in consectetur. Vestibulum pharetra lectus eu porta consectetur. Nulla consectetur libero odio, quis vehicula magna aliquam nec. Donec scelerisque tellus ac risus eleifend tincidunt.</p>',
				'type'			=>	1,
				'user_id'		=>	1,
				'created_at'	=>	new DateTime,
				'updated_at'	=>	new DateTime
		));
	}
}