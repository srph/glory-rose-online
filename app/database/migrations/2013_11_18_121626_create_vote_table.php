<?php

use Illuminate\Database\Migrations\Migration;

class CreateVoteTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vote', function($table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('link_id');
			$table->date('created_at');
			$table->date('updated_at')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vote');
	}

}