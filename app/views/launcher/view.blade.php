@extends('launcher.master')

@section('content')
<h3> ({{ News::defineType($item->type) }}) {{ $item->title }} </h3>
{{ $item->body }}

<a href="{{ URL::route('launcher_index') }}">Back</a>
@stop