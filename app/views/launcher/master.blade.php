<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/fonts.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/default_2.css') }}" />
</head>

<body>
	@yield('content')
</body>
</html>