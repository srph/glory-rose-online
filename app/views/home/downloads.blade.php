@extends('layout.master')

@section('content')

	{{-- Sidebar --}}
	@include('layout.widget.side')

	<div class="grid_8">
			<div class="body">

		<h1> Downloads </h1>
		<p> You can choose between two options to download the whole game for your gaming platform. Pick one that suits you best. </p>

		<div class="button">
			Full Client Download
		</div>

		<p>
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sit amet placerat erat, sit amet vehicula ipsum. Mauris pretium, nibh eu tincidunt viverra, odio ante tincidunt urna, vel consequat odio libero sed dui. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam varius varius metus et adipiscing. Mauris vel purus ac felis varius cursus. Morbi ut arcu id nibh interdum blandit ac vel urna. Aliquam luctus, nisl ut faucibus congue, arcu eros aliquet ante, non pretium justo augue ac leo. Proin id tristique quam. Sed ac leo tortor. Morbi ultricies, est eu auctor ullamcorper, dui dolor consequat odio, quis adipiscing metus purus in mi. Pellentesque sodales urna massa, non fermentum lacus egestas id. Aenean in sapien ornare nisi sagittis dapibus. Cras nec metus massa.
		</p>

		<p>
		Aliquam erat volutpat. Pellentesque at molestie ipsum. Etiam in eros interdum ligula suscipit imperdiet eu ut leo. Curabitur quis elit in odio vehicula placerat a 	cursus purus. Donec odio est, pellentesque ut eleifend id, iaculis in elit. Suspendisse id risus convallis risus posuere dictum. Phasellus lectus nibh, suscipit vitae consequat ut, auctor eu tellus. Morbi fermentum augue lorem, eget fermentum purus rhoncus at. Integer neque metus, facilisis eget lorem vel, aliquam commodo enim. Praesent arcu magna, sagittis sit amet augue eu, malesuada aliquam libero. Quisque ac nisl at nibh porttitor dictum vitae vitae urna.
		</p>

		</div>
	</div>

@stop