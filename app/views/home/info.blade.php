@extends('layout.master')

@section('content')

	{{-- Sidebar --}}
	@include('layout.widget.side')

	{{-- Main --}}
	<div class="grid_8">
		<div class="body">
				<h1> Server Information </h1>

				<p>Aenean metus nunc, ultricies et lobortis a, elementum lobortis eros. Vestibulum id nibh ut augue laoreet rhoncus ultricies vel enim. Curabitur dignissim neque placerat, tempor est at, adipiscing arcu. Aliquam erat volutpat. Maecenas blandit dignissim dui ac varius. Fusce non fringilla leo. Maecenas ac pulvinar massa. Aliquam sed tellus non est ultricies laoreet. Phasellus sit amet pharetra sem, quis suscipit tellus. Fusce lorem massa, tempor eu magna vel, luctus placerat lorem. </p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas volutpat mauris et ipsum luctus venenatis. Nunc ut elit a risus hendrerit elementum consectetur id ipsum. Sed lobortis pharetra purus, at sollicitudin lectus venenatis aliquam. Nullam rhoncus lacus lacus, id vulputate neque volutpat eget. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin dignissim vel mauris in consectetur. Vestibulum pharetra lectus eu porta consectetur. Nulla consectetur libero odio, quis vehicula magna aliquam nec. Donec scelerisque tellus ac risus eleifend tincidunt.</p>


				<h3> Team & Staff </h3>
				<p>Aenean metus nunc, ultricies et lobortis a, elementum lobortis eros. Vestibulum id nibh ut augue laoreet rhoncus ultricies vel enim. Curabitur dignissim neque placerat, tempor est at, adipiscing arcu. Aliquam erat volutpat. Maecenas blandit dignissim dui ac varius. Fusce non fringilla leo. Maecenas ac pulvinar massa. Aliquam sed tellus non est ultricies laoreet. Phasellus sit amet pharetra sem, quis suscipit tellus. Fusce lorem massa, tempor eu magna vel, luctus placerat lorem. </p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas volutpat mauris et ipsum luctus venenatis. Nunc ut elit a risus hendrerit elementum consectetur id ipsum. Sed lobortis pharetra purus, at sollicitudin lectus venenatis aliquam. Nullam rhoncus lacus lacus, id vulputate neque volutpat eget. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin dignissim vel mauris in consectetur. Vestibulum pharetra lectus eu porta consectetur. Nulla consectetur libero odio, quis vehicula magna aliquam nec. Donec scelerisque tellus ac risus eleifend tincidunt.</p>

				<h3> Why is it fun to stay in Glory Rose? </h3>
				<p>Aenean metus nunc, ultricies et lobortis a, elementum lobortis eros. Vestibulum id nibh ut augue laoreet rhoncus ultricies vel enim. Curabitur dignissim neque placerat, tempor est at, adipiscing arcu. Aliquam erat volutpat. Maecenas blandit dignissim dui ac varius. Fusce non fringilla leo. Maecenas ac pulvinar massa. Aliquam sed tellus non est ultricies laoreet. Phasellus sit amet pharetra sem, quis suscipit tellus. Fusce lorem massa, tempor eu magna vel, luctus placerat lorem. </p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas volutpat mauris et ipsum luctus venenatis. Nunc ut elit a risus hendrerit elementum consectetur id ipsum. Sed lobortis pharetra purus, at sollicitudin lectus venenatis aliquam. Nullam rhoncus lacus lacus, id vulputate neque volutpat eget. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin dignissim vel mauris in consectetur. Vestibulum pharetra lectus eu porta consectetur. Nulla consectetur libero odio, quis vehicula magna aliquam nec. Donec scelerisque tellus ac risus eleifend tincidunt.</p>

				{{-- Clan Ranking --}}
				<h2> Clan Ranking </h2>
				<table>
					<tr>
						<td> # </td>
						<td> Clan </td>
						<td> Grade </td>
					</tr>

					@foreach($clans as $index => $clan)
						<tr>
							<td> {{ $index + 1 }} </td>
							<td> {{$clan->name}} </td>
							<td> {{$clan->grade}} </td>
						</tr>
					@endforeach
				</table>

				<h2> Character Ranking </h2>
				<table>
					<tr>
						<td> # </td>
						<td> Character </td>
						<td> Level </td>
					</tr>

					@foreach($characters as $index => $character)
						<tr>
							<td> {{ $index + 1 }} </td>
							<td> {{$character->char_name}} </td>
							<td> {{$character->level}} </td>
						</tr>
					@endforeach
				</table>
		</div>
	</div>

@stop