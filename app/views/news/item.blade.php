@extends('layout.master')

@section('content')

	{{-- Sidebar --}}
	@include('layout.widget.side')

	{{-- Main --}}
	<div class="grid_8">
		<div class="body">
				<h3> ({{ News::defineType($item->type) }}) {{ $item->title }} </h3>
				<h5> {{ date('d/m/Y', strtotime($item->created_at)) }} </h5>
					{{ $item->body }}

				<a href="{{ URL::route('home') }}"> Back </a>
		</div>
	</div>

@stop