@extends('layout.master')

@section('content')

	{{-- Sidebar --}}
	@include('layout.widget.side')

	{{-- Main --}}
	<div class="grid_8">
		<div class="body">
				<h3> Post news </h3>
				@include('layout.message')
				{{ Form::open(array('route' => 'news_create', 'method' => 'POST')) }}
					{{ Form::token()}}
					{{ Form::select('type', array(1 => 'Announcements', 2 => 'Event'))}}
					{{ Form::text('title', NULL, array('placeholder' => 'Title')) }}
					{{ Form::textarea('body') }}
					{{ Form::submit('Post News') }}
				{{ Form::close() }}
		</div>
	</div>

@stop