@extends('layout.master')

@section('content')
	{{-- Sidebar --}}
	@include('layout.widget.side')

	{{-- Main --}}
	<div class="grid_8">
		<div class="body">

		@if ( Session::has('message') || Session::has('error') )
			@include('layout.message')
		@else
			<h1> Reactivate your email </h1>
			<p> Before asking for a reactivation, please check your spam mails. Sometimes, our servers may be overloaded that our mails are delayed. Anyway, please enter the e-mail address of your respective account </p>
			{{ Form::open(array('route' => 'reactivate')) }}
				{{ Form::token() }}
				{{ Form::text('email') }}
				{{ Form::submit('Send Reactivation Code!') }}
			{{ Form::close() }}
		@endif
		</div>
	</div>

@stop