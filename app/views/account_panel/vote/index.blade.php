@extends('layout.master')

@section('content')
	{{-- Sidebar --}}
	@include('layout.widget.side')

	{{-- Main --}}
	<div class="grid_8">
		<div class="body">
			<h1> Vote 4 Points </h1>
			@if ( Session::has('error') )
				@include('layout.message')
			@else
				<div id="message" class="message">
					<h3> Rewards! </h3>
				 	<p>Simply click a button to vote. In exchange, we will reward you for 2 points for each vote. You may only vote once per link every 12 hours! </p>
				</div>
			@endif

			<p class="text-center">  You currently have <span class="alizarin"> {{ $user->vote_points }} </span> points </p>
			<div align="center">
				<a id="scheme_one" href="{{ URL::route('vote_update', 1) }}" class="vote">
					VOTE FOR US @ <br /> XTREMETOP 100
				</a>

				<a id="scheme_two" href="{{ URL::route('vote_update', 2) }}" class="vote">
					 VOTE FOR US @ <br /> GAMESTOP 100
				</a>
			</div>

		</div>
	</div>
@stop