@extends('layout.master')

@section('content')
	{{-- Main --}}
	<div class="grid_12">
		<div class="body">
	<h1> Create your account! </h1>
	@include('layout.message')
	{{ Form::open(array('route' => 'create', 'method' => 'POST')) }}
		{{ Form::token() }}
		{{ Form::text('username', NULL, array('placeholder' => 'Username')) }}
		{{ Form::password('password', array('placeholder' => '•••••••')) }}
		{{ Form::password('password_confirmation', array('placeholder' => '•••••••', 'title' => 'Die')) }}
		{{ Form::text('email', NULL, array('placeholder' => 'example@gloryrose.com')) }}
		{{ Form::text('email_confirmation', NULL, array('placeholder' => 'example@gloryrose.com')) }}
		{{ Form::submit('Create Account') }}
	{{ Form::close() }}
		</div>
	</div>
@stop