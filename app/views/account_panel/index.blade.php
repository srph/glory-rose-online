@extends('layout.master')

@section('content')
	{{-- Sidebar --}}
	@include('layout.widget.side')

	{{-- Main --}}
	<div class="grid_8">
		<div class="body">
	<h1> Dashboard </h1>
	@include('layout.message')
	{{ Form::open() }}
		<label> Username </label>
		{{ Form::text('username', Auth::user()->username, array('disabled' => 'disabled')) }}
		{{ Form::password('password', array('disabled' => 'disabled', 'placeholder' => Auth::user()->password)) }}
		{{ Form::text('email', Auth::user()->email, array('disabled' => 'disabled')) }}

		<a href="{{ URL::route('update') }}"><div class="button"> Change Password! </div></a>
	{{ Form::close()}}
		</div>
	</div>
@stop