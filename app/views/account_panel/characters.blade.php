@extends('layout.master')


@section('content')

	{{-- Sidebar --}}
	@include('layout.widget.side')

	{{-- Main --}}
	<div class="grid_8">
		<div class="body">

			<h1> Your Characters </h1>

			@if ( User::isActive(Auth::user()) )

				@if ( count($characters) == 0 )
					<p> You have no character yet! </p>
				@else
					<table>
						<tr>
							<td> Character </td>
							<td> Level </td>
							<td> Class </td>
							<td> Clan </td>
						</tr>

						@foreach($characters as $character)
						<tr>
							<td> {{ $character->char_name }} </td>
							<td> {{ $character->level }} </td>
							<td> {{ Character::defineClass($character->classid) }} </td>
							<td> {{ Clan::define($character->clanid) }} </td>
						</tr>
						@endforeach
					</table>
				@endif

			@else
				@include('layout.message')

			@endif
		</div>
	</div>
@stop