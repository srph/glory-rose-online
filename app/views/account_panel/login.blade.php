@extends('layout.master')

@section('content')
	{{-- Main --}}
	<div class="grid_12">
		<div class="body">

		<h1> Account Panel Login </h1>
			@include('layout.message')
			{{ Form::open(array('route' => 'login', 'method' => 'POST')) }}
				{{ Form::token() }}
				{{ Form::text('username', NULL, array('placeholder' => 'Username')) }}
				{{ Form::password('password', array('placeholder' => '•••••••')) }}
				{{ Form::submit('Login!') }}
			{{ Form::close() }}
		</div>
	</div>
@stop