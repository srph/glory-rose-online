@extends('layout.master')

@section('content')
	{{-- Sidebar --}}
	@include('layout.widget.side')

	{{-- Main --}}
	<div class="grid_8">
		<div class="body">
				<h1> Change Password </h1>
				@include('layout.message')

			{{ Form::open(array('route' => 'update', 'method' => 'PUT')) }}
				<label> Old Password </label>
				{{ Form::password('old_password', array('placeholder' => '•••••••')) }}
				<label> New Password </label>
				{{ Form::password('new_password', array('placeholder' => '•••••••')) }}
				{{ Form::password('new_password_confirmation', array('placeholder' => '•••••••')) }}
				{{ Form::submit('Change Password') }}
			{{ Form::close()}}
		</div>
	</div>
@stop