<div class="grid_12">
	<div class="menu">

		<ul class="navigation">
			<li><a href="{{ URL::route('home') }}">Home</a></li>
			@if ( Auth::guest() ) <li><a href="{{ URL::route('create') }}">Register</a></li> @endif
			<li><a href="{{ URL::route('downloads') }}">Downloads</a></li>
			<li><a href="{{ URL::route('forum') }}">Community</a></li>
			<li><a href="{{ URL::route('info') }}">Server Info</a></li>
		</ul>

		<div class="status">
			@include('layout.widget.status')
		</div>

	</div>
</div>