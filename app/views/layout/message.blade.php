@if ( Session::has('success') )
	<div id="success" class="message">
		<h3> Success! </h3>
		<p> {{ Session::get('success') }} </p>
	</div>
@elseif ( Session::has('error') )
	<div id="error" class="message">
		<h3> Error! </h3>
		<p> {{ Session::get('error') }} </p>
	<!--<ul>
		@foreach($errors->all() as $error)
			<li> {{ $error }} </li>
		@endforeach
	</ul>-->
	</div>
@elseif ( Session::has('message') )
	<div id="message" class="message">
		<h3> Message </h3>
		<p> {{ Session::get('message') }} </p>
	</div>
@elseif( Session::has('warning') )
	<div id="warning" class="message">
		<h3> Success! </h3>
		<p> {{ Session::get('warning') }} </p>
	</div>
@endif

@if ( Auth::check() && !User::isActive(Auth::user()) )
	<div id="warning" class="message">
		<h3> Warning! </h3>
		<p> Your account has not yet been activated. Please activate your account to play the game. </p>
	</div>
@endif