@if ( isset($news) )
		<table>			
			<tr>
				<td> ID </td>
				<td> Title </td>
				<td> Date </td>
			</tr>

			@foreach($news as $item)
					<tr>
						<td>{{ $item->id }} </td>
						<td><a href="{{ URL::route('news_item', $item->id) }}">{{ $item->title }}</a></td>
						<td> {{ date('m/d/Y', strtotime($item->created_at)) }} </td>
					</tr>
			@endforeach
		</table>
@endif