<div class="grid_4">
	@if ( Auth::guest() )
		@include('layout.widget.login')
	@else
		@include('layout.widget.panel')
	@endif

	@if ( Auth::check() && Auth::user()->accesslevel >= 200 )
		@include('layout.widget.admin')
	@endif

	@if ( Auth::guest() )
		@include('layout.widget.vote')
	@endif
</div>