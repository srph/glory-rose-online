<div class="widget">
	<h3> Welcome, User! </h3>
	{{ Form::open(array('route' => 'login' ,'method' => 'POST')) }}
		{{ Form::token()}}
		{{ Form::text('username', NULL, array('placeholder' => 'Username')) }}
		{{ Form::password('password', array('placeholder' => '•••••••••')) }}
		{{ Form::submit('Login') }}
	{{ Form::close() }}
	<p> <a href="{{ URL::route('create') }}"> Not a member yet? </a> </p>
	<p> <a href="{{ URL::route('activate') }}"> Haven't received your activation? </a> </p>
</div>