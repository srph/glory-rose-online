<div class="widget">
	<h3> Account Panel </h3>
	<ul>
		<li id="dashboard"> <a href="{{ URL::route('account_panel') }}"> Dashboard </a> </li>
		<li id="characters"> <a href="{{ URL::route('characters') }}"> View Characters </a> </li>
		<li id="mall"> <a href="#"> Item Mall </a> </li>
		<li id="vote"> <a href="{{ URL::route('vote') }}"> Vote 4 Points </a> </li>
		<li id="logout"> <a href="{{ URL::route('logout') }}"> Logout </a> </li>
	</ul>
</div>