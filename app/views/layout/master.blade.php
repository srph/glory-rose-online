<!DOCTYPE html>
<html lang="en">
<head>
	<title> {{ Config::get('server.title') }} - {{ $title }}</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<meta name="author" content="SRPH, Glory ROSE Online" />
	<meta name="keywords" content="Glory ROSE Online, ROSE Online, Rush On Seven Episodes" />
	<meta name="description" content="Glory ROSE Online is a mid-rate private server with stable run-time, updated stuffs and cosmetics, and a good community" />
	<link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('favicon.ico') }}" />
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/fonts.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/960_12_col.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/flexslider.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/default_2.css') }}" />
</head>

<body>

	<div class="container_12">

	<img id="logo" src="{{ URL::asset('img/logo.png') }}" />

		{{-- Menu --}}
		@include('layout.menu')

		{{-- Main --}}
		@yield('content')

	</div>


	{{-- Footer --}}
	<div class="footer">
		<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sit amet placerat erat, sit amet vehicula ipsum </p>
	</div>

	{{-- Scripts --}}
	@include('layout.scripts')


</body>
</html>
