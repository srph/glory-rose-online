<?php

class News extends Eloquent {
	public $timestamps = true;
	protected $fillable = array( 'title', 'body', 'user_id', 'type', 'created_at', 'updated_at' );

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'news';

	/**
	 * ORM with users
	 *
	 * @return void
	 */
	public static function users()
	{
		$this->belongsTo('user');
	}

	/**
	 * Get thumbnail of a certain news item
	 *
	 * @param 	$id 	int
	 * @return 	string
	 */
	public static function getThumb($id)
	{
		return URL::asset('img/news/thumbnails/' . $id . '.jpg')	;
	}

	/**
	 *
	 *
	 *
	 */
	public static function defineType($id)
	{
		switch($id) {
			case 1:
				return 'Announcement';
			case 2:
				return 'Event';
			case 3:
				return 'Maintenance';
			default:
				return false;
		}
	}
	
}