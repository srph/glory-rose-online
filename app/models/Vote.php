<?php

class Vote extends Eloquent {
	public $timestamps = true;
	public $fillable = array( 'user_id', 'link_id', 'created_at', 'updated_at');
	protected $table = 'vote';

	public static function linkExists($id, $user)
	{
		$link_exists = Vote::where('user_id', '=', $user)->where('link_id', '=', $id)->first(); // Query the row
		return ( $link_exists ) ? true : false; // If link exists, return true otherwise false
	}

	public static function user()
	{
		$this->belongsTo('user');
	}
}