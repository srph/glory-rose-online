<?php

class Clan extends Eloquent {

	protected $table = 'list_clan';
	public static function user()
	{
		$this->hasMany('users');
	}

	/**
	 * Define the name of the clan id
	 *
	 * @param 	$id 	int
	 * @return 	string
	 */
	public static function define($id)
	{
		$clan = Clan::where('id', '=', $id)->first();
		return ( count($clan) == 0 ) ? 'No clan' : $clan->name;
	}

	public static function rank()
	{
		$clan =  Clan::orderBy('grade', 'desc')->take(10)->get();
		foreach($clans as $clan) {
			$i++;
		}
	}
}