     <?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	public $timestamps = false;

	protected $fillable = array( 'username', 'password', 'email' , 'active', 'active_key', 'vote_points' );

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'accounts';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	/**
	 * Send activation to user's email
	 *
	 * @param 	$id 	int
	 * @return void
	 */
	public static function sendActivation($id)
	{
		$user =  self::where('id', '=', $id)->first();

		// If user is already active
		if ( $user->active == 1 ) {
			Session::flash('error', 'We are sorry your account is already active!'); // Flash an error
			return Redirect::route('activate'); // Redirect the user
		}

		$hash = $user->active_key; // User's hash
		$to = $user->email; // User's email
		$subject = 'Glory ROSE Online - Activate your account!'; // Subject
		$message = '

		Thank you for signing up for an account in our server. Your account has been created, however, you may only
		login after you have activated your account by pressing the url below.

		Please click this link to activate your account:

		http://localhost/rose-project/public/activate/' . $id . '/' . $hash; // Message to the user

		$headers = 'From:noreply@gloryrose.com'; // Set our headers

		mail($to, $subject, $message, $headers); //Send the email
	}

	/**
	 * Check if the user is already active
	 *
	 * @param 	int 	$active
	 * @return 	bool
	 */
	public static function isActive($user)
	{
		return ( $user->active == 1 ) ? true : false; //If the user is not active, return true. Otherwise, false.
	}

	/**
	 * Validate registration input
	 *
	 * @param	array 	$input
	 * @return	bool
	 */
	public static function validateRegistration($input)
	{
		$rules = array(
			'username'	=>	'required|between:4,16|alpha_dash|unique:accounts,username',
			'password'	=>	'required|between:6,32|alpha_dash|confirmed',
			'password_confirmation'	=>	'required|between:6,32|alpha_dash',
			'email'	=>	'required|email|confirmed|unique:accounts,email',
			'email_confirmation'	=>	'required|email'
		);	// Validation Rules

		return Validator::make($input, $rules);	// Validate
	}

	/**
	 * Validate user's input
	 *
	 * @param	array 	$input
	 * @return	bool
	 */
	public static function validateUpdate($input)
	{
		$rules = array(
			'new_password'	=>	'required|between:6,32|alpha_dash|confirmed',
			'new_password_confirmation'	=>	'required|alpha_dash|between:6,32',
		);	// Validation Rules

			return Validator::make($input, $rules);	// Validate
	}

	/**
	 * Check if user has voted since the given time
	 *
	 * @param 	int 	$link_id
	 * @return 	bool
	 */
	public static function hasVoted($link)
	{
		$interval = (  strtotime(date('Y-m-d h:i:s')) - strtotime($link->updated_at) ); // Calculate interval between current date and last vote
		$interval = $interval / 12 / 30 / 24; // Format the interval
		return ( $interval < 0.5 ) ? true : false;
	}

	public static function news()
	{
		$this->hasMany('news');
	}
	
	public static function characters()
	{
		$this->hasMany('characters');
	}

	public static function votes()
	{
		$this->hasMany('votes');
	}

}