<?php

class Character extends Eloquent {
	
	public static function clan()
	{
		$this->hasOne('clan');
	}

	public static function user()
	{
		$this->belongsTo('user');
	}

	/**
	 * Define the class id of a character
	 *
	 * @param 	$id 	int
	 * @return 	string
	 */
	public static function defineClass($id)
	{
		switch($id) {
			case 1:
				return 'Visitor';
			case 2:
				return 'Swordsman';
			case 3:
				return 'Hawker';
			case 4:
				return 'Magician';
			case 5:
				return 'Clarity';
			default:
				return 'Classless';
		}
	}

}