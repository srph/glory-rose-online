<?php

/**
 * Redirect users trying to access a staff-only content
 *
 */
Route::filter('staff', function()
{
	if ( Auth::guest() ) {
		Session::flash('error', 'You need to be logged in to access the content'); // Flash an error
		return Redirect::route('login'); // Redirect
	}

	if ( Auth::user()->accesslevel < 200 ) {
		Session::flash('error', 'You have insufficient rights to access the page!'); // Flash an error
		return Redirect::guest('home'); // Redirect
	}
});