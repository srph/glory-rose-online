<?php

class Status {

	public static function up()
	{
		$ip = Config::get('server.ip');
		$ports = Config::get('server.ports');

		// Check each port if it is open and used
		foreach($ports as $port_name => $port) {
			// If a port is down, return false
			$port_up = @fsockopen($ip, $port, $errno, $errstr, 30);
			if ( !$port_up ) return false;
		}

		// Otherwise, true
		return true;
	}

	public static function void()
	{
		return true;
	}

}
