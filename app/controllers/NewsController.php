<?php

class NewsController extends BaseController {

	public function __construct()
	{
		$this->beforeFilter('staff', array('only' => array('getCreate')));
	}

	public function getCreate()
	{
		return View::make('news.create')->with('title', 'Post new announcement!');
	}

	/**
	 * Create a news item
	 *
	 * @return 	void
	 */
	public function postCreate()
	{
		// Create the news
		$news_created = News::create(array(
			'type'			=>	Input::get('type'),
			'title'			=>	Input::get('title'),
			'body'			=>	Input::get('body'),
			'user_id'		=>	Auth::user()->id,
			'created_at'	=>	new DateTime,
			'updated_at'	=>	new DateTime
		));

		if ( $news_created ) {
			Session::flash('success', 'You have successfully posted the news item'); // Flash a success message
			Redirect::route('news', News::orderBy('id', 'desc')->first()); // Redirect the user to the page
		}

		Session::flash('error', 'An error occured while posting the news item!'); // Flash an error
		Redirect::route('news_create'); // Refresh the page
	}

	/**
	 * View a certain news item
	 *
	 * @param 	$id 	int
	 * @return 	void
	 */
	public function getView($id)
	{
		$item = News::where('id', '=', $id)->first();
		return View::make('news.item')->with(array('title' => $item->title, 'item' => $item));
	}

	/**
	 *
	 *
	 *
	 */
}