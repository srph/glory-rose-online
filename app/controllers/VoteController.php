<?php

class VoteController extends BaseController {

	/**
	 * All filters to be used in current controller
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->beforeFilter('auth');
	}

	// Show the page
	public function getIndex()
	{
		return View::make('account_panel.vote.index')->with(array('title' => 'Vote 4 Points', 'user' => User::where('id', '=', Auth::user()->id)->first()));
	}

	/**
	 * Add points to user and redirect
	 *
	 * @param 	int 	@$id
	 */
	public function getVote($id)
	{
		$user = Auth::user(); // Current authenticated / logged in user's id
		$link = Vote::where('user_id', '=', $user->id)->where('link_id', '=', $id)->first(); // Query the row

		// If link does not exist, create the link
		if ( !Vote::linkExists($id, $user) ) {
			// Create the link
			Vote::create(array(
				'user_id'		=>	$user->id,
				'link_id'		=>	$id,
				'created_at'	=>	new DateTime,
				'updated_at'	=>	new DateTime
			));
		}	// Konti na lang, tatanggalin na kita. Cause Putangina mo that's why.
			// ba't ayaw mo ba magmultiple creation eloquent gago ka tangina mo gagogagogaogaogagoo

		switch($id) {
			case 1:
				$url = Config::get('server.vote_links.link1');
				break;

			case 2:
				$url = Config::get('server.vote_links.link2');
				break;

			default:		// If the id (url) does not exist
				Session::flash('error', 'Vote link does not exist'); // Flash an error
				return Redirect::route('vote'); // Redirect
		}

		// If the user has voted in this link in the past 12 hours
		if ( User::hasVoted($link) ) {
			Session::flash('error', 'You have already voted since the last 12 hours'); // Flash an error
			return Redirect::route('vote'); // Redirect
		}

		$user = User::where('id', '=', $user->id)->first(); // Grab the user
			/**
			 * TODO
			 * 
			 * If a certain IP has already voted a certain link,
			 * block him from voting until a given time has passed.
			 *
			 */
		$point = $user->vote_points + Config::get('server.vote_links.point'); // Point
		$user->update(array('vote_points' => $point)); // Increase the user's vote points
		$link->update(array('updated_at' => new DateTime)); // Set its last vote to current time
		return Redirect::route('vote_redirect', $id); // Redirect to link
	}

	/**
	 * Redirect the user
	 *
	 * @param 	int 	@$id
	 */
	public function getRedirect($id)
	{
			switch($id) {
			case 1:
				$url = Config::get('server.vote_links.link1'); // Grab link
				return Redirect::to($url); // Redirect

			case 2:
				$url = Config::get('server.vote_links.link2'); // Grab link
				return Redirect::to($url); // Redirect

			default:		// If the id (url) does not exist
				Session::flash('error', 'Vote link does not exist'); // Flash an error
				return Redirect::route('vote'); // Redirect
		}
	}
}