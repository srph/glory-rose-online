<?php

class HomeController extends BaseController {

	/**
	 * Return the home page
	 *
	 * @return void
	 */
	public function getIndex()
	{
		return View::make('home.index')->with(array('title' => 'Home', 'news' => News::orderBy('id', 'desc')->take(10)->get()));
	}

	public function getDownloads()
	{
		return View::make('home.downloads')->with('title', 'Downloads');
	}

	public function getForum()
	{
		return Redirect::to('/forum');
	}

	public function getInfo()
	{
		return View::make('home.info')->with(array(
			'title' => 'Server Information',
			'clans' => Clan::orderBy('grade', 'desc')->take(10)->get(),
			'characters' => Character::orderBy('level', 'desc')->take(10)->get()
		));
	}

}