<?php

class AccountPanelController extends BaseController {

	/**
	 * All filters to be used in current controller
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->beforeFilter('csrf', array('only' => array('postCreate', 'postLogin', 'postUpdate')));
		$this->beforeFilter('guest', array('only' => array('getLogin', 'getCreate')));
		$this->beforeFilter('auth', array('only' => array('getIndex', 'getUpdate', 'getCharacters', 'getLogout')));
	}

	public function getIndex()
	{
		return View::make('account_panel.index')->with('title', 'Account Panel');
	}

	public function getCreate()
	{
		return View::make('account_panel.create')->with('title', 'Create your account!');
	}

	/**
	 * Create the user
	 *
	 * @return void
	 */
	public function postCreate()
	{
		// Validate user's input
		$validation = User::validateRegistration(Input::all());

		// If validation passess
		if ( $validation->passes() ) {
			// Create the user
			$user = User::create(array(
				'username'		=>	Input::get('username'),
				'password'		=>	Hash::make(Input::get('password')),
				'email'			=>	Input::get('email'),
				'active'		=>	0,
				'active_key'	=>	Str::random(40)
			));

			// Create the vote links for the user

			Vote::create(array(
					'user_id'		=>	$user->id,
					'link_id'		=>	1,
					'created_at'	=>	new DateTime
			));

			Vote::create(array(
					'user_id'		=>	$user->id,
					'link_id'		=>	2,
					'created_at'	=>	new DateTime
			));

			User::sendActivation($user->id); // Send an activation

			Session::flash('message', 'Congratulations! We have created your account. To activate, please follow the instructions from the message we sent to the email you provided!'); // Flash a notification
			return Redirect::route('activate');	// Redirect the user to the account panel
		}

		// Otherwise,
		Session::flash('error', 'An error occured while registering!'); // Flash an error
		return Redirect::route('create')->withErrors($validation); // Redirect to registration page with validation errors
	}

	public function getLogin()
	{
		return View::make('account_panel.login')->with('title', 'Sign in!');
	}

	/**
	 * Login the user
	 *
	 * @return void
	 */
	public function postLogin()
	{
		$user = array(
			'username'	=>	Input::get('username'),
			'password'	=>	Input::get('password')
		); // Grab user's input

		// Attempt to login the user
		if ( Auth::attempt($user) ) {
			// If login was successful,
			Session::flash('success', 'You have logged in successfully'); // Flash a success message
			return Redirect::route('account_panel'); // Return him to the account panel
		}

		// Otherwise, 
		Session::flash('error', 'Your username / password combination was incorrect!'); // Flash an error
		return Redirect::route('login'); // Redirect him to the login page
	}

	/**
	 * Logout the user
	 *
	 * @return void
	 */
	public function getLogout()
	{
		Auth::logout(); // Logout the user
		Session::flash('success', 'You have been logged out!'); // Flash success a message
		return Redirect::route('login'); // Redirect
	}

	public function getUpdate()
	{
		return View::make('account_panel.update')->with('title', 'Change Password');
	}

	/**
	 * Update user password
	 *
	 * @return void
	 */
	public function putUpdate()
	{

		// Validate if the user entered the exact password as the current password
		if ( !(Hash::check(Input::get('old_password'), Auth::user()->password)) ) {
			Session::flash('error', 'You entered an incorrect password! Please try again.');
			return Redirect::route('update');
		}

		// Validate
		$validation = User::validateUpdate(Input::all());
		if ( $validation->passes() ) {

			// Update his information with current input
			$user = User::where('id', '=', Auth::user()->id)->first();

			// If update was success
			if ( $user ) {
				$user->update(array('password'	=> Hash::make(Input::get('new_password'))));
				Auth::logout(); // Logout
				Session::flash('success', 'You have updated your account successfully. Please login again to access the account panel!'); // Flash a message
				return Redirect::route('login'); // Redirect him to the account panel index.
			}
		}

		// Otherwise,
		Session::flash('error', 'An error has occurred!'); // Flash an errorr
		return Redirect::route('update')->withErrors($validation);
	}

	/**
	 * Query the characters of the logged in account
	 *
	 */
	public function getCharacters()
	{

		$username = Auth::user()->username;
		return View::make('account_panel.characters')->with(array(
			'title' => 'Your Characters',
			'characters' => Character::where('account_name', '=', $username)->get()
		));
	}

}