<?php

/**
 * View for the game launcher
 *
 */
class LauncherController extends BaseController {

	public function getIndex()
	{
		$news = News::orderBy('id', 'desc')->take(10)->get();
		return View::make('launcher.index')->with('news', $news);
	}

	public function getView($id)
	{
		$item = News::where('id', '=', $id)->first();
		return View::make('launcher.view')->with('item', $item);
	}
}