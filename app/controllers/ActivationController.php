<?php

class ActivationController extends BaseController {

	public function getIndex()
	{
		return View::make('account_panel.activate')->with('title', 'Activate your account!');
	}

	public function postIndex()
	{
		$email = Input::get('email');
		$user = User::where('email', '=', $email)->first();

		// If an account exists with the provided email
		if ( $user ) {
			User::sendActivation($user->id); // Send the activation

			Session::flash('message', 'Your activation code has been sent to your email!'); // Notify the user
			return Redirect::route('activate'); // Redirect the user
		}

		// Otherwise
		Session::flash('error', 'We are sorry but no accounts exists with the email you provided'); // Flash an error
		return Redirect::route('activate'); // Redirect
	}

	/**
	 * Activate user's account upon access
	 *
	 * @param 	int 	$id
	 * @param 	string 	$hash
	 * @return 	void
	 */
	public function getActivate($id, $key)
	{
		$user = User::where('id', '=', $id)->first();

		if ( $user ) {
			// Validate the hash
			if ( $user->active_key !== $key ) {
				// If the hash is invalid, bail the user
				Session::flash('error', 'You have entered an invalid hash!'); // Flash an error
				return Redirect::route('activate'); // Redirect the user to the activation page
			}

			// Validate if the user's account is already active
			if ( $user->active ) {
				Session::flash('error', 'We are sorry your account is already active!'); // Flash an error
				return Redirect::route('activate'); // Redirect the user to the activation page
			}

			$user->update(array('active' => 1)); // Activate his email

			Auth::login($user);	// Login the user
			Session::flash('success', 'Congratulations! Your account has been activated succesfully.');	// Send a message
			return Redirect::route('account_panel'); // Redirect the user to the account panel
		}

		Session::flash('error', 'No such account'); // Flash an error
		return Redirect::route('activate'); // Redirect the user to the activation page
	}

}