<?php

return array(

	/**
	 * Web Title
	 *
	 */
	'title'	=>	'ROSE Online',
	'rates' => array(
		'drops'	=>	'25x',
		'item'	=>	'20x',
		'level'	=>	'50x'
	),

	'vote_links' => array(
		'point' => '2',
		'link1'	=>	'http://gtop.com/',
		'link2' =>	'http://xtop.com/'
	),

	/**
	 * IP of the server
	 *
	 */
	'ip'	=>	'192.168.1.2',


	/**
	 * Ports used by the server
	 *
	 */
	'ports' => array(
		'login'	=>	'29000',
		'char'	=>	'29100',
		'world'	=>	'29200'
	)
);