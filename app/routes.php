<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/* Pattern */
Route::pattern('id', '[0-9]+');

/* Home */
Route::get('/', array('uses' => 'HomeController@getIndex', 'as' => 'home'));
Route::get('downloads', array('uses' => 'HomeController@getDownloads', 'as' => 'downloads'));
Route::get('info', array('uses' => 'HomeController@getInfo', 'as' => 'info'));
Route::get('community/forums', array('uses' => 'HomeController@getForum', 'as' => 'forum'));

/* Account Panel */
Route::get('account-panel', array('uses' => 'AccountPanelController@getIndex', 'as' => 'account_panel'));
	// Registration
Route::get('account-panel/create', array('uses' => 'AccountPanelController@getCreate', 'as' => 'create'));
Route::post('account-panel/create', array('uses' => 'AccountPanelController@postCreate'));

	// Activation
Route::get('account-panel/activate', array('uses' => 'ActivationController@getIndex', 'as' => 'activate'));
Route::post('account-panel/activate/resend', array('uses' => 'ActivationController@postIndex', 'as' => 'reactivate')); // Resend
Route::get('account-panel/activate/{id}/{key}', array('uses' => 'ActivationController@getActivate')); // Activate the email

	// Login	
Route::get('account-panel/login', array('uses' => 'AccountPanelController@getLogin', 'as' => 'login'));
Route::post('account-panel/login', array('uses' => 'AccountPanelController@postLogin'));

	// Update
Route::get('account-panel/update', array('uses' => 'AccountPanelController@getUpdate', 'as' => 'update'));
Route::put('account-panel/update', array('uses' => 'AccountPanelController@putUpdate'));

	// Characters
Route::get('account-panel/characters', array('uses' => 'AccountPanelController@getCharacters', 'as' => 'characters'));

	//Logout
Route::get('account-panel/logout', array('uses' => 'AccountPanelController@getLogout', 'as' => 'logout'));

	// Vote
Route::get('account-panel/vote', array('uses' => 'VoteController@getIndex', 'as' => 'vote'));
Route::get('account-panel/vote/{id}', array('uses' => 'VoteController@getVote', 'as' => 'vote_update'));
Route::get('account-panel/vote/{id}/go', array('uses' => 'VoteController@getRedirect', 'as' => 'vote_redirect'));

/* News */
Route::get('news/{id}', array('uses' => 'NewsController@getView', 'as' => 'news_item'));
Route::get('news/create', array('uses' => 'NewsController@getCreate', 'as' => 'news_create'));
Route::post('news/create', array('uses' => 'NewsController@postCreate'));
	// Launcher
Route::get('launcher', array('uses' => 'LauncherController@getIndex', 'as' => 'launcher_index'));
Route::get('launcher/{id}', array('uses' => 'LauncherController@getView', 'as' => 'launcher_view'));